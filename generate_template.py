from jinja2 import Template
import sys

print(f"args: {sys.argv}")

cluster_name = sys.argv[1]
account_id= sys.argv[2]
with open("cluster_config.j2") as _file:
    template = Template(_file.read())

with open("eks_template.yml", "w") as fh:
    fh.write(template.render(cluster_name=cluster_name, ACCOUNT_ID=account_id))