EXTERNAL_IP=$1
SAMPLE_TRAFFIC_NAMESPACE=eks-test-nginx-ingress
curl https://raw.githubusercontent.com/aws-samples/amazon-cloudwatch-container-insights/master/k8s-deployment-manifest-templates/deployment-mode/service/cwagent-prometheus/sample_traffic/nginx-traffic/nginx-traffic-sample.yaml | \
sed "s/{{external_ip}}/$EXTERNAL_IP/g" | \
sed "s/{{namespace}}/$SAMPLE_TRAFFIC_NAMESPACE/g" | \
kubectl apply -f -

kubectl get pod -n $SAMPLE_TRAFFIC_NAMESPACE