CLUSTER_NAME=$1
NODE_COUNT=$2
VOLUME_SIZE=$3
AWS_REGION=$4
echo "passed in params : cluster name = $CLUSTER_NAME , how many nodes = $NODE_COUNT size of node's volume = $VOLUME_SIZE"
eksctl create cluster \
--name $CLUSTER_NAME \
--version 1.17 \
--region $AWS_REGION \
--nodegroup-name "$CLUSTER_NAME-ng-1-workers" \
--node-type t2.micro \
--nodes $NODE_COUNT \
--node-volume-size $VOLUME_SIZE
--color fabulous
# --ssh-access 


# create service account to manage ingress controller and helm
# eksctl utils associate-iam-oidc-provider --cluster=$CLUSTER_NAME --region=$AWS_REGION --color=fabulous --approve
# eksctl create iamserviceaccount \
# --cluster=$CLUSTER_NAME \
# --name="$CLUSTER_NAME-service-account" \
# --region $AWS_REGION \
# --attach-policy-arn=arn:aws:iam::628605195423:policy/AWSLoadBalancerControllerIAMPolicy \
# --color=fabulous \
# --override-existing-serviceaccounts \
# --approve
